package PalindromoOCJP;

import java.util.*;
import java.util.regex.*;

public class PalindromoOCJP {

        public static void main(String[] args) {

            Scanner sc= new Scanner(System.in);

            System.out.print("Introduce una frase (puede tener puntos, comas y espacios): ");
            String sCadena=sc.nextLine();

            //aqui me reemplaza caracteres especiales
            sCadena=sCadena.replace(" ", "");
            sCadena=sCadena.replace(",", "");
            sCadena=sCadena.replace(".", "");

            //System.out.print(sCadena);

            System.out.println(sCadena.toLowerCase());
            //tomamos uno menos de toda la longitud del string
            int fin = sCadena.length()-1;
            int ini=0;

            boolean espalin=true;

            while(ini < fin){

                if(sCadena.charAt(ini) != sCadena.charAt(fin)){
                    espalin=false;
                }
                ini++;
                fin--;
            }
            if(espalin)
                System.out.print("\nEs palindromo.");
            else
                System.out.print("\nNo es palindromo.");
        }

   /* public static String remove2(String input) {
        // Descomposición canónica
        String normalized = Normalizer.normalize(input, Normalizer.Form.NFD);
        // Nos quedamos únicamente con los caracteres ASCII
        Pattern pattern = Pattern.compile("\\P{ASCII}+");
        return pattern.matcher(normalized).replaceAll("");
    }*/

}
